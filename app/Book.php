<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public $fillable = ['title', 'description', 'cover', 'author_id'];

    public function author(){
        return $this->belongsTo('App\Author');
    }

    public function tags(){
        return $this->belongsToMany('App\Tags', 'book_tag', 'book_id', 'tag_id');
    }
}
