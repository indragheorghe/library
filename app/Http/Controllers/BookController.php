<?php

namespace App\Http\Controllers;

use App\Tags;
use App\Author;
use Illuminate\Http\Request;
use App\Http\Requests\CreateNewBookRequest;
use Illuminate\Support\Facades\Redirect;
use App\Book;
use App\Http\Requests;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        foreach($books as $key => $book){
           $book->author;
           $book->tags;
           $books[$key] = $book;
        }

        return view("library.book")->withActive('book')->withBooks($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tags::all();
        $tags_id = $tags->pluck('id');
        $tags_name = $tags->pluck('name');

        $authors = Author::all();
        $authors_id = $authors->pluck('id');
        $authors_name = $authors->pluck('name');

        return view("library.book_create")->withActive('book')
            ->withTags(array_combine($tags_id->toArray(), $tags_name->toArray()))
            ->withAuthors(array_combine($authors_id->toArray(), $authors_name->toArray()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewBookRequest $request)
    {

        $request = $request->all();

        $destinationPath = 'uploads'; // upload path
        $extension = $request['image']->getClientOriginalExtension(); // getting image extension
        $fileName = str_random(8).'.'.$extension; // renameing image
        $request['image']->move($destinationPath, $fileName); // uploading file to given path

        $book = new Book();
        $book->title = $request['title'];
        $book->description = $request['description'];
        $book->cover = $fileName;
        $book->author_id = $request['author'];
        $book->save();

        $book->tags()->attach($request['tags']);

        $author = Author::find($request['author']);
        $book->author()->associate($author);
        return Redirect::to('book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        $tags = Tags::all();
        $tags_id = $tags->pluck('id');
        $tags_name = $tags->pluck('name');

        $authors = Author::all();
        $authors_id = $authors->pluck('id');
        $authors_name = $authors->pluck('name');

        return view("library.book_edit")->withActive('book')
            ->withBook($book)
            ->withTags(array_combine($tags_id->toArray(), $tags_name->toArray()))
            ->withAuthors(array_combine($authors_id->toArray(), $authors_name->toArray()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);

        $request = $request->all();

        if(isset($request['image'])) {
            $destinationPath = 'uploads'; // upload path
            $extension = $request['image']->getClientOriginalExtension(); // getting image extension
            $fileName = str_random(8) . '.' . $extension; // renameing image
            $request['image']->move($destinationPath, $fileName); // uploading file to given path
        }

        $book->title = $request['title'];
        $book->description = $request['description'];
        $book->cover = $fileName;
        $book->author_id = $request['author'];
        $book->save();

        $book->tags()->attach($request['tags']);

        $author = Author::find($request['author']);
        $book->author()->associate($author);

        return Redirect::to('book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book= Book::find($id);
        unlink('uploads/'.$book->cover);
        $book->delete();
        return Redirect::to('book');
    }
}
