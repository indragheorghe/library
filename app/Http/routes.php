<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function(){
   return view('library.home');
});

Route::get('author', 'AuthorController@index');
Route::get('author/create', 'AuthorController@create');
Route::post('author/create', 'AuthorController@store');
Route::get('author/edit/{id}', 'AuthorController@edit');
Route::patch('author/{id}/edit', 'AuthorController@update');
Route::get('author/destroy/{id}', 'AuthorController@destroy');

Route::get('book', 'BookController@index');
Route::get('book/create', 'BookController@create');
Route::post('book/create', 'BookController@store');
Route::get('book/edit/{id}', 'BookController@edit');
Route::patch('book/{id}/edit', 'BookController@update');
Route::get('book/destroy/{id}', 'BookController@destroy');

Route::get('tags', 'TagsController@index');
Route::get('tags/create', 'TagsController@create');
Route::post('tags/create', 'TagsController@store');
Route::get('tags/edit/{id}', 'TagsController@edit');
Route::patch('tags/{id}/edit', 'TagsController@update');
Route::get('tags/destroy/{id}', 'TagsController@destroy');
