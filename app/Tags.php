<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    public $fillable = ['name', 'description'];

    public function Books()
    {
        return $this->hasMany('App\Books');
    }
}
