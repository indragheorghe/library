<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Library</title>
    {!! HTML::style('css/bootstrap.min.css') !!}
</head>
<body>
    @extends('library.navbar')
<div class="container" style="margin-top: 50px;">
    @yield('content')
</div>
    @yield('footer')

{!! HTML::script('js/jquery-3.1.0.min.js') !!}
{!! HTML::script('js/bootstrap.min.js') !!}
</body>
</html>