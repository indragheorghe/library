@extends('library.main')

@section('content')
    <h1>Authors</h1>
    <p><a class="btn btn-primary btn-sm" href="{{action("AuthorController@create")}}" role="button">Add new &raquo;</a></p>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($authors as $author)

        <tr>
            <td class="col-md-3">{{$author->name}}</td>
            <td class="col-md-6">{{$author->description}}</td>
            <td class="col-md-3">
                <a href="{{action("AuthorController@edit", $author->id)}}">Edit</a>
                <a href="{{action("AuthorController@destroy", $author->id)}}">Delete</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop