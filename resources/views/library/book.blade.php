@extends('library.main')

@section('content')
    <h1>Books</h1>
    <p><a class="btn btn-primary btn-sm" href="{{action("BookController@create")}}" role="button">Add new &raquo;</a></p>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Cover</th>
            <th>Title</th>
            <th>Author</th>
            <th>Tags</th>
            <th>Description</th>
            <th>Actions</th>

        </tr>
        </thead>
        <tbody>
        @foreach($books as $book)
        <tr>
            <td class="col-md-2"><img src="uploads/{!! htmlspecialchars($book->cover) !!} "  class="img-responsive"/> </td>
            <td class="col-md-2">{{$book->title}}</td>
            <td class="col-md-2">{{$book->author->name}}</td>
            <td>
                @foreach($book->tags as $tag)
                        {{$tag->name}},
                @endforeach
            </td>
            <td class="col-md-6">{{$book->description}}</td>
            <td class="col-md-3">
                <a href="{{action("BookController@edit", $book->id)}}">Edit</a>
                <a href="{{action("BookController@destroy", $book->id)}}">Delete</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop