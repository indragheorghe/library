<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Library</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li @if(isset($active) && $active=='book') {!! 'class="active"'!!} @endif>
                    <a href="{{action("BookController@index")}}">Books</a>
                </li>
                <li @if(isset($active) && $active=='author') {!! 'class="active"'!!} @endif>
                    <a href="{{action("AuthorController@index")}}">Authors</a>
                </li>
                <li @if(isset($active) && $active=='tags') {!! 'class="active"'!!} @endif>
                    <a href="{{action("TagsController@index")}}">Tags</a>
                </li>
            </ul>
        </div>
    </div>
</nav>