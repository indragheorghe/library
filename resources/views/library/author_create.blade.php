@extends('library.main')

@section('content')
    <h1>Add new author</h1>
    <hr/>

    <div class="form-group">
        <!-- forma urmeaza sa fie aici -->

        {!! Form::open(['method'=>'POST', 'action'=> 'AuthorController@store']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::input('text', 'name', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <br>
            {!! Form::textarea('description', null, null, ['class'=>'form-control',
            'cols'=>'80', 'rows'=>'10']) !!}
        </div>
        {!! Form::submit('Save', ['class'=>'btn btn-secondary btn-sm']) !!}
        {!! HTML::link(action("AuthorController@index"), 'cancel') !!}
        {!! Form::close() !!}
    </div>

    @if(isset($errors))
        @if(count($errors)>0)
            <ul class="alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        @endif
    @endif
@stop