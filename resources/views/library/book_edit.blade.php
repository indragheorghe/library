@extends('library.main')

@section('content')
    <h1>Add new author</h1>
    <hr/>

    <div class="form-group">
        <!-- forma urmeaza sa fie aici -->

        {!! Form::model($book, ['method'=>'PATCH', 'action'=> ['BookController@update', $book->id],  'files'=>true]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::input('text', 'title', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('author', 'Author:') !!}
            <br>
            {!!Form::select('author', $authors, $book->author_id,   ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tags', 'Tags:') !!}
            <br>
            {!!Form::select('tags[]', $tags, null,  ['class'=>'form-control', 'multiple' => true, 'id' => 'tags']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <br>
            {!! Form::textarea('description', null, null, ['class'=>'form-control',
            'cols'=>'80', 'rows'=>'10']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('image', 'Cover:') !!}
            <br>
            {!! Form::file('image') !!}
        </div>
        {!! Form::submit('Save', ['class'=>'btn btn-secondary btn-sm']) !!}
        {!! HTML::link(action("BookController@index"), 'cancel') !!}
        {!! Form::close() !!}
    </div>

    @if(isset($errors))
        @if(count($errors)>0)
            <ul class="alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        @endif
    @endif
@stop