@extends('library.main')

@section('content')
    <h1>Tags</h1>
    <p><a class="btn btn-primary btn-sm" href="{{action("TagsController@create")}}" role="button">Add new &raquo;</a></p>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)

        <tr>
            <td class="col-md-3">{{$tag->name}}</td>
            <td class="col-md-6">{{$tag->description}}</td>
            <td class="col-md-3">
                <a href="{{action("TagsController@edit", $tag->id)}}">Edit</a>
                <a href="{{action("TagsController@destroy", $tag->id)}}">Delete</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop